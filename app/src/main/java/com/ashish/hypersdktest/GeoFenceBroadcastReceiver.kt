package com.ashish.hypersdktest

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.Location
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.google.android.gms.location.*
import com.google.android.gms.tasks.CancellationTokenSource
import com.hypertrack.sdk.HyperTrack
import com.hypertrack.sdk.persistence.database.AccountDataStore.PUBLISHABLE_KEY
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class GeoFenceBroadcastReceiver  :
    BroadcastReceiver() {
    private lateinit var geoFencingClient: GeofencingClient
    private var geoFenceArrayList: ArrayList<Geofence> = ArrayList()
    private lateinit var geoFencePendingIntent: PendingIntent

    var location: Location? = null
    private lateinit var sdkInstance: HyperTrack


    @SuppressLint("MissingPermission")
    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "onReceive: Called ")
       Log.d(TAG, "onReceive: Trip Monitoring Initiated ")
        val geoFencingEvent = GeofencingEvent.fromIntent(intent)
        geoFencingClient = LocationServices.getGeofencingClient(context)
        if (geoFencingEvent != null) {
            if (geoFencingEvent.hasError()) {
                val errorMessage = GeofenceStatusCodes
                    .getStatusCodeString(geoFencingEvent.errorCode)
                Log.d(TAG, errorMessage)
                return
            }
        }

        geoFencePendingIntent = getPendingIntent(context)

        // Get the transition type.
        val geoFenceTransition = geoFencingEvent?.geofenceTransition

        // Test that the reported transition was of interest.
        if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
            geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT
        ) {
            Log.d(TAG, "onReceive: Event Occured $geoFenceTransition")
            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            val triggeringGeofences = geoFencingEvent.triggeringGeofences

            // Get the transition details as a String.
            val geofenceTransitionDetails = triggeringGeofences?.let {
                getGeoFenceTransitionDetails(
                    geoFenceTransition,
                    it
                )
            }
            if (geofenceTransitionDetails != null) {
                Log.d(TAG, geofenceTransitionDetails)
            }
            if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
           sdkInstance = HyperTrack.getInstance(PUBLISHABLE_KEY)

                Toast.makeText(context, "We exit from this location", Toast.LENGTH_SHORT).show()
                removeGeoFences()
                if (geoFencingEvent.triggeringLocation!!.accuracy > 400f) {
                    Log.d(
                        TAG,
                        "Fetching New Location since the accuracy of the GeoFence Triggering location is more than 150"
                    )
                    fetchLocation(context)
                } else populateGeoFenceList(geoFencingEvent.triggeringLocation)
            }
//            geofenceCallback.removeAndAddGeofence(
//                geofencingEvent.triggeringLocation,
//                geofenceTransitionDetails
//            )

//            // Send notification and log the transition details.
//            sendNotification(geofenceTransitionDetails, context)
            if (geofenceTransitionDetails != null) {
                Log.i(TAG, geofenceTransitionDetails)
            }
        } else {
            // Log the error.
            Log.d(
                TAG, "Geofence Transition invalid type $geoFenceTransition"
            )
        }
    }

    @SuppressLint("MissingPermission")
    private fun fetchLocation(context: Context) {
        val fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(context)
        try {
            fusedLocationProviderClient.getCurrentLocation(
                LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY,
                CancellationTokenSource().token
            ).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    location = task.result
                    location?.let { loc ->
                        loc.accuracy.apply {
                            if (this > 150f) {
                                fetchLocation(context)
                            } else
                                populateGeoFenceList(loc)
                        }
                    }
                } else {
                   Log.d(TAG, "Location Fetching Failure $task.exception")
                }
            }
        } catch (e: Exception) {
            e.message?.let { Log.d(TAG, it) }
        }

    }



    private fun getPendingIntent(context: Context): PendingIntent {
        val geofencePendingIntent: PendingIntent by lazy {
            Log.d(TAG, "Pending Intent Called  ")
            val i = Intent(context, GeoFenceBroadcastReceiver::class.java)
            // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
            // addGeofences() and removeGeofences().
            PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT)
        }
        return geofencePendingIntent
    }

    /**
     * Maps geofence transition types to their human-readable equivalents.
     *
     * @param transitionType A transition type constant defined in Geofence
     * @return A String indicating the type of transition
     */
    private fun getTransitionString(transitionType: Int): String {
        return when (transitionType) {
            Geofence.GEOFENCE_TRANSITION_ENTER -> "Geofence Transition entered"
            Geofence.GEOFENCE_TRANSITION_EXIT -> "Geofence Transition exit"
            Geofence.GEOFENCE_TRANSITION_DWELL -> "Geofence Transition Dwell"
            else -> "Unknown Geofence Transition"
        }
    }

    private fun populateGeoFenceList(location: Location?) {
        geoFenceArrayList.clear()
        if (location != null) {
            Log.d(TAG, "populateGeofenceList: $location")
            geoFenceArrayList.add(
                Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId("IntuGEOFence")

                    // Set the circular region of this geofence.
                    .setCircularRegion(location.latitude, location.longitude, 350F)

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(
                        Geofence.GEOFENCE_TRANSITION_ENTER or
                                Geofence.GEOFENCE_TRANSITION_EXIT
                    )
                    .build()
            )
            addGeoFences()
        }
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private fun getGeoFencingRequest(): GeofencingRequest {
        // Return a GeofencingRequest.
        Log.d(TAG, "getGeofencingRequest: Called ")
        return GeofencingRequest.Builder().apply {
            // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
            // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
            // is already inside that geofence.
            setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            // Add the geofences to be monitored by geofencing service.
            addGeofences(geoFenceArrayList)
        }.build()
    }


    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */


    @SuppressLint("MissingPermission")
    private fun addGeoFences() {
        geoFencingClient.addGeofences(getGeoFencingRequest(), geoFencePendingIntent).run {
            addOnSuccessListener {
                // Geofences added
                // ...
                Log.d(TAG, "addGeofences: Geofences Added Successfully")
            }
            addOnFailureListener {
                // Failed to add geofences
                // ...
                Log.d(
                    TAG,
                    "addGeofences: Failed to add Geofences reason: ${it.message}",
                )
            }
        }
    }

    private fun removeGeoFences() {
        geoFencingClient.removeGeofences(geoFencePendingIntent).run {
            addOnSuccessListener {
                // Geofences removed
                // ...
                Log.d(TAG, "removeGeofences: Geofence Removed Successfully")
            }
            addOnFailureListener {
                // Failed to remove geofences
                Log.d(
                    TAG,
                    "removeGeofences: Failure : ${it.message} and the throwable is $it"
                )
            }
        }
    }

    /**
     * Gets transition details and returns them as a formatted string.
     *
     * @param geoFenceTransition  The ID of the geofence transition.
     * @param triggeringGeofences The geofence(s) triggered.
     * @return The transition details formatted as String.
     */
    private fun getGeoFenceTransitionDetails(
        geoFenceTransition: Int,
        triggeringGeofences: List<Geofence>
    ): String {
        val geoFenceTransitionString = getTransitionString(geoFenceTransition)

        // Get the Ids of each geofence that was triggered.
        val triggeringGeoFencesIdsList = ArrayList<String>()
        for (geoFence in triggeringGeofences) {
            triggeringGeoFencesIdsList.add(geoFence.requestId)
        }
        val triggeringGeoFencesIdsString = TextUtils.join(", ", triggeringGeoFencesIdsList)
        return "$geoFenceTransitionString: $triggeringGeoFencesIdsString"
    }

    companion object {
        private const val TAG = "GeofenceBroadReceiver"
        private const val CHANNEL_ID = "channel_01"
    }
}