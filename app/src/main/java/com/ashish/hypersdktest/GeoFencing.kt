package com.ashish.hypersdktest

import android.Manifest
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.location.LocationServices

class GeoFencing(val context: Context) {
    private var geoFencingClient: GeofencingClient = LocationServices.getGeofencingClient(context)
    private var geoFenceArrayList = ArrayList<Geofence>()

//    fun getGeoFenceUpdate(location: Location?){
//    }

    fun populateGeoFenceList(location: Location?) {
        geoFenceArrayList.clear()
        removeGeofences()
        if (location != null) {
            geoFenceArrayList.add(
                Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId("Intermediate Ring Rd")

                    // Set the circular region of this geofence.
                    .setCircularRegion(location.latitude, location.longitude, 100F)

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(
                        Geofence.GEOFENCE_TRANSITION_ENTER or
                                Geofence.GEOFENCE_TRANSITION_EXIT
                    )
                    .build()
            )
            geoFenceArrayList.add(
                Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId("IntuGEOFence")

                    // Set the circular region of this geofence.
                    .setCircularRegion(location.latitude, location.longitude, 200F)

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(
                        Geofence.GEOFENCE_TRANSITION_ENTER or
                                Geofence.GEOFENCE_TRANSITION_EXIT
                    )
                    .build()
            )
            geoFenceArrayList.add(
                Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId("IntuGEOFence")

                    // Set the circular region of this geofence.
                    .setCircularRegion(location.latitude, location.longitude, 300F)

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(
                        Geofence.GEOFENCE_TRANSITION_ENTER or
                                Geofence.GEOFENCE_TRANSITION_EXIT
                    )
                    .build()
            )

            addGeoFences()
        }
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private fun getGeofencingRequest(): GeofencingRequest {
        // Return a GeofencingRequest.
       Log.d(TAG, "getGeofencingRequest: Called ")
        return GeofencingRequest.Builder().apply {
            // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
            // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
            // is already inside that geofence.
            setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            // Add the geofences to be monitored by geofencing service.
            addGeofences(geoFenceArrayList)
        }.build()
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */

    private val geoFencePendingIntent: PendingIntent by lazy {
      Log.d(TAG, "Pending Intent Called  ")
        val intent = Intent(context, GeoFenceBroadcastReceiver::class.java)
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_IMMUTABLE)
    }

    private fun addGeoFences() {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.d(TAG, "addGeoFences: Location Permissions are not granted")
            return
        }
        geoFencingClient.addGeofences(getGeofencingRequest(), geoFencePendingIntent).run {
            addOnSuccessListener {
                // Geofences added
                // ...
                Log.d(TAG, "addGeofences: Geofences Added Successfully")
            }
            addOnFailureListener {
                // Failed to add geofences
                // ...
                Log.d(
                    TAG,
                    "addGeofences: Failed to add Geofences reason: ${it.message} exception: $it"
                )
            }
        }
    }

    private fun removeGeofences() {
        geoFencingClient.removeGeofences(geoFencePendingIntent).run {
            addOnSuccessListener {
                // Geofences removed
                // ...
                Log.d(TAG, "removeGeofences: Geofence Removed Successfully")
            }
            addOnFailureListener {
                // Failed to remove geofences
                // ...
                Log.d(
                    TAG,
                    "removeGeofences: Failure : ${it.message} exception is $it"
                )
            }
        }
    }


    companion object {
        private const val TAG = "GeoFencing"
    }
}