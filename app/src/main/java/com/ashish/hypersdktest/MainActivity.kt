package com.ashish.hypersdktest

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.hypertrack.sdk.HyperTrack
import com.hypertrack.sdk.ServiceNotificationConfig
import com.hypertrack.sdk.TrackingError
import com.hypertrack.sdk.TrackingError.*
import com.hypertrack.sdk.TrackingStateObserver

class MainActivity : AppCompatActivity() , TrackingStateObserver.OnTrackingStateChangeListener {

    private lateinit var trackingStatus: Button
    private lateinit var sdkInstance: HyperTrack
   private lateinit var longitude : TextView
   private lateinit var latitude : TextView
   private lateinit var geoFencing: GeoFencing
   private lateinit var geoFenceBroadcastReceiver: GeoFenceBroadcastReceiver

    private var shouldAskPermissions = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG, "onCreate")

        sdkInstance = HyperTrack
            .getInstance(PUBLISHABLE_KEY)
            .addTrackingListener(this)

        trackingStatus = findViewById(R.id.track_status)
        val deviceId = findViewById<TextView>(R.id.device_id)
        deviceId.text = sdkInstance.deviceID
        Log.d(TAG, "device id is ${sdkInstance.deviceID}")

        trackingStatus.setOnClickListener {
            if(sdkInstance.isRunning) {
                sdkInstance.stop()
            } else {
                sdkInstance.start()
            }
        }
        sdkInstance.setTrackingNotificationConfig(
            ServiceNotificationConfig.Builder()
                .setContentTitle("This app is started")
                .build()
        )
        geoFenceBroadcastReceiver = GeoFenceBroadcastReceiver()
        Log.d(TAG, "onCreate:${geoFenceBroadcastReceiver.location?.longitude}")
        Log.d(TAG, "onCreate:${geoFenceBroadcastReceiver.location?.latitude}")

    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")

        when {
            sdkInstance.isRunning -> onTrackingStart()
            else -> onTrackingStop()
        }

        if (shouldAskPermissions) {
            shouldAskPermissions = false
            sdkInstance.requestPermissionsIfNecessary()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
        sdkInstance.removeTrackingListener(this)
    }



    override fun onError(trackingError: TrackingError?) {
        Log.d(TAG, "onError ${trackingError?.message}")

        when (trackingError?.code){
            GPS_PROVIDER_DISABLED_ERROR ->
                trackingStatus.text =getString(R.string.diabled_error)
            INVALID_PUBLISHABLE_KEY_ERROR, AUTHORIZATION_ERROR ->
                trackingStatus.text = getString(R.string.key_error)
            PERMISSION_DENIED_ERROR ->
                trackingStatus.text = getString(R.string.denied_error)
            else ->
                trackingStatus.text = getString(R.string.cant_start)
        }
        trackingStatus.setBackgroundColor(Color.WHITE)
    }

    @SuppressLint("SetTextI18n")
    override fun onTrackingStart() {
        Log.d(TAG, "onTrackingStart")
        geoFencing = GeoFencing(this)
        longitude = findViewById(R.id.lon)
        latitude = findViewById(R.id.lat)

        val location = sdkInstance.latestLocation
        if (location.isSuccess) {
            Log.d(TAG, "onTrackingStart Longitude - ${location.value.longitude}")
            Log.d(TAG, "onTrackingStart Latitude -  ${location.value.latitude}")
            longitude.text = location.value.longitude.toString()
            latitude.text = location.value.latitude.toString()

            geoFencing.populateGeoFenceList(location.value)

        } else {
            Log.d(TAG, "onTrackingStart no data comming form SDK")
            longitude.text = "No value"
            latitude.text = "No value"
        }
            trackingStatus.setBackgroundColor(Color.GREEN)
            trackingStatus.text = getString(R.string.tracking_started)
    }
    override fun onTrackingStop() {
        Log.d(TAG, "onTrackingStop")

        trackingStatus.setBackgroundColor(Color.RED)
        trackingStatus.text = getString(R.string.traking_stopped)
    }

    companion object {
        private const val TAG = "MainActivity"
        private const val PUBLISHABLE_KEY = "oFBt82XWZgNLh74atOE9PtqfDFzr5J2uWWbTaTKKBGG6PyfdJIrL2LYtAdCazQlnAJcFYsOSHdTkMTbr52sQpA"
    }
}